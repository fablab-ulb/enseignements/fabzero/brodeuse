# Tutoriel

# Présentation de la machine

Brother F480 : Machine à coudre **ET** à broder

![](img/Innov-is-F480.jpg)

Le [manuel](https://download.brother.com/welcome/doch100732/888l01_l03_k02_om01fr.pdf) de la machine est très bien fait.

# Précautions

* Ne pas déplacer la machine lorsque l'unité de broderie est branchée.
* Utiliser la machine sur une table dégagée, l'unité doit pouvoir se déplacer sans encombre.

# Étape 1 : Préparation du fichier

* Le fichier doit être un fichier vectorisé.
* Ouvrir votre fichier dans Inkscape et dimensionnez le correctement.
* Télécharger et installer [l'extension InkStitch](https://inkstitch.org/fr/)
* Dans l'onglet extension, aller dans le menu deroulant Inkstitch, paramétrer votre broderie en fonction de vos besoin
* N'hésitez pas à faire un tour sur leur [site](https://inkstitch.org/fr/tutorials/resources/beginner-video-tutorials/) pour plus de précision
* Une fois les paramétrages bons, aller dans le menu Fichier->Enregistrer sous et selectionner le fichier .pes
* Mettre le fichier sur une clé USB

# Étape 2 : Préparation de la machine
## Étape 2.1 : Montage de l'unité de broderie

* Retirer l'unité de couture et insérer l'unité de broderie en prenant soin de maintenir le levier de sécurité.
![](img/BROD_1.png)
* Allumer la machine, si vous avez un doute sur les différentes manipulations de la machine, utiliser le menu d'aide de la machine.
![](img/BROD_2.png)
![](img/BROD_3.png)

## Étape 2.2 : Enfilage inférieur

![](img/BROD_4.png)

* Procéder au bobinage de la canette. [Section 2 du menu d'aide]
* Installer la canette dans dans la machine. [Section 3 du menu d'aide]

## Étape 2.3 : Enfilage supérieur

* Procéder à l'enfilage supérieur. [Section 1 du menu d'aide]

## Étape 2.4 : Changement de l'aiguille

* Installer une aiguille adaptée a votre tissus. Celle-ci varie entre 75 et 90 en fonction de la rigidité de votre tissus. [Section 4 du menu d'aide]

## Étape 2.5 : Installation du pied de broderie

* Installer le pied de broderie. [Section 7 du menu d'aide]

# Étape 3 : Préparation du cadre de broderie

## Étape 3.1 : Préparation du tissus

* Préparer le tissus selon les indications de la page B58 du manuel

## Étape 3.2 : Fixation du cadre de broderie

* Fixer le cadre de broderie selon les indications de la page B62 du manuel

# Étape 4 : Selection de la broderie

## Étape 4.1 : Importer le fichier

* Importer le fichier a l'aide de l'entrée USB sur le coté droit de la machine.
* Appuyer sur le bouton **Placer**
* Régler les dimensions, sens et les couleurs de fils
* Appuyer sur le bouton **Fin Edit.**
* Définir l'emplacement de la broderie
* Appuyer sur le bouton **Broderie**

# Étape 5 : Broder

* Abaisser le pied de broderie et appuyer sur le bouton Marche/Arret


# Lien utils

[Manuel](https://support.brother.com/g/b/manualtop.aspx?c=be&lang=fr&prod=hf_inovf480euk)

[Video YT](https://www.youtube.com/watch?v=VkKzaDuEOWM)

[Video YT](https://www.youtube.com/watch?v=7cdl4pTVpx8)